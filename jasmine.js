const codeframe = require("html-validate/build/formatters/codeframe");

function toBeValid() {
	return {
		compare
	};
}

function compare(report, context) {
	const result = { pass: report.valid };
	if (report.valid) {
		result.message = "Expected validation errors but there was none";
	} else {
		const errors = codeframe(report.results);
		const capitalized = context
			? `: ${context[0].toUpperCase()}${context.substr(1)}`
			: "";
		const prefix = `html-validate${capitalized}:\n`;
		result.message = `${prefix}${errors}`;
	}
	return result;
}

/* add jasmine matchers */
beforeAll(() => {
	jasmine.addMatchers({
		toBeValid
	});
});

/* validate page when leaving test */
afterEach(() => {
	expect(browser.htmlvalidate()).toBeValid("after test finished");
});
