module.exports = {
	extends: ["htmlvalidate:recommended", "htmlvalidate:document"],

	rules: {
		/* firefox does not retain doctype when returning the source code */
		"missing-doctype": "off",

		/* the browser will often do what it wants, out of users control */
		void: "off",

		/* some frameworks (such as jQuery) often uses inline style, e.g. for
		 * showing/hiding elements */
		"no-inline-style": "off",

		/* scripts will often add markup with trailing whitespace */
		"no-trailing-whitespace": "off",

		/* browser might normalize boolean attributes */
		"attribute-boolean-style": "off"
	}
};
