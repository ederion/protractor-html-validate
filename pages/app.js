(function() {
	const testComponent = {
		template: `<p>Test component</p>
<button id="toggle-invalid" type="button" ng-click="showInvalid = !showInvalid" ng-init="showInvalid = false">Show invalid</button>
<div ng-if="showInvalid">
<button><p>Invalid permitted permitted content</p></button>
</div>
`
	};

	const invalidComponent = {
		template: `<button type="button"><p>Invalid permitted permitted content</p></button>`
	};

	const hiddenComponent = {
		template: `<div>hidden text</div>`,
		controller: function($element) {
			this.$onInit = () => {
				$element.find("div")[0].style.display = "none";
			};
		}
	};

	angular
		.module("app", [])
		.component("testComponent", testComponent)
		.component("invalidComponent", invalidComponent)
		.component("hiddenComponent", hiddenComponent);
})();
