/**
 * Run protractor against local webserver.
 *
 * Some of these tests will fail and the results will be written to
 * `protractor-result.json` which `results.spec.js` will read and verify the
 * expected results.
 */
describe("protractor-html-validate", () => {
	const toggleInvalid = element(by.id("toggle-invalid"));

	it("should not report error when a valid page is loaded", () => {
		browser.get("/valid.html");
	});

	it("should report error when invalid page is loaded", () => {
		browser.get("/invalid.html");
	});

	it("should report error if page is invalid when test is finished", () => {
		browser.get("/valid.html");
		toggleInvalid.click();
	});

	it("should not report error if page is valid at start and beginning", () => {
		browser.get("/valid.html");
		toggleInvalid.click();
		toggleInvalid.click();
	});

	it("should support manual validation", () => {
		browser.get("/valid.html");
		toggleInvalid.click();
		expect(browser.htmlvalidate()).toBeValid();
		toggleInvalid.click();
	});
});
