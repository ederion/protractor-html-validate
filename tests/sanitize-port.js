/* since protractor will run on any available port it must be stripped from the
 * output or the test will fail during the next run because the port changed */
module.exports = {
	test: () => true,
	print: str => {
		return str.replace(/http:\/.*?\//g, "http://localhost/");
	}
};
