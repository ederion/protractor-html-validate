/* load protractor results and create a flat object with full spec name -> spec */
const protractorResults = require(global.resultsFilename); /* set from {chrome,firefox}.spec.js */
const protractorSpecs = Object.values(protractorResults).reduce(
	(out, suite) => {
		for (const spec of suite.specs) {
			out[spec.fullName] = spec;
		}
		return out;
	},
	{}
);

describe("protractor-html-validate", () => {
	let protractorSpec;

	beforeAll(() => {
		/* for each spec load the corresponding result from protractor */
		jasmine.getEnv().addReporter({
			specStarted: function(spec) {
				protractorSpec = protractorSpecs[spec.fullName];
			}
		});
	});

	beforeEach(() => {
		expect(protractorSpec).toBeDefined();
	});

	it("should not report error when a valid page is loaded", () => {
		expect(protractorSpec.status).toEqual("passed");
	});

	it("should report error when invalid page is loaded", () => {
		expect(protractorSpec.status).toEqual("failed");
		expect(protractorSpec.failedExpectations).toHaveLength(
			2
		); /* two since it is invalid when test is finished */
		expect(protractorSpec.failedExpectations[0].message).toMatchSnapshot();
		expect(protractorSpec.failedExpectations[1].message).toMatchSnapshot();
	});

	it("should report error if page is invalid when test is finished", () => {
		expect(protractorSpec.status).toEqual("failed");
		expect(protractorSpec.failedExpectations).toHaveLength(1);
		expect(protractorSpec.failedExpectations[0].message).toMatchSnapshot();
	});

	it("should not report error if page is valid at start and beginning", () => {
		expect(protractorSpec.status).toEqual("passed");
	});

	it("should support manual validation", () => {
		expect(protractorSpec.status).toEqual("failed");
		expect(protractorSpec.failedExpectations).toHaveLength(1);
		expect(protractorSpec.failedExpectations[0].message).toMatchSnapshot();
	});
});
