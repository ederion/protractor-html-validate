# Protractor html-validate plugin

Validates HTML using `html-validate` before, during and after tests. It
automatically fetches the active source markup from the browser and validates,
failing the test if any validation errors is encountered. Manual checks can be
added as needed.

**Note: currently only jasmine is supported.**

- Each `browser.get(..)` triggers a validation.
- Each test triggers a validation after running (`afterEach`).
- Manual validations via `browser.htmlvalidate()`.

## Usage

### In `protractor.conf.js`:

```js
{
    plugins: [
        /* load plugin */
        {package: 'protractor-html-validate'}
    ],

    onPrepare: () => {
        /* load jasmine helper */
        require('protractor-html-validate/jasmine');
    }
}
```

### In specs:

Each `browser.get(..)` and `afterEach` will trigger a validation.

To manually verify use `expect(browser.htmlvalidate()).toBeValid()`, e.g.:

```js
it("should be valid", () => {
	myPage.clickButton(); /* shows something */
	expect(browser.htmlvalidate()).toBeValid();
	myPage.clickAnotherButton(); /* hides something */
});
```

## Configuration

`html-validate` configuration can be passed in `protractor.conf.js`:

```js
{
    plugins: [
        {package: 'protractor-html-validate', config: {
            plugins: [
                'my-fancy-plugin',
            ],
            rules: {
                'foo': 'error',
            },
        }}
    ],
}
```

## Example

If loading a page with invalid markup the test will automatically fail:

```
Failures:
1) my test case
  Message:
    html-validate: When loading page:
    error: Input element does not have a label (input-missing-label) at http:/127.0.0.1:36749/invalid.html:10:4:
       8 | 		<h1>Invalid template</h1>
       9 |
    > 10 | 		<input type="text" id="attr-test" readonly="" required="" disabled="disabled" />
         | 		 ^^^^^
      11 |
      12 |
      13 | </body></html>


    1 error found.
```
