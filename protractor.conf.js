const express = require("express");
const serveStatic = require("serve-static");
const JSONReporter = require("jasmine-json-test-reporter");

const app = express();
let server;

/* start a temporary express server */
function beforeLaunch() {
	return new Promise((resolve, reject) => {
		app.use(serveStatic("node_modules/angular"));
		app.use(serveStatic("pages"));
		server = app.listen(0, "localhost", err => {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});
}

async function onPrepare() {
	const addr = server.address();
	browser.baseUrl = `http://${addr.address}:${addr.port}`;

	/* save results as json */
	jasmine.getEnv().clearReporters();
	const caps = await browser.getCapabilities();
	jasmine.getEnv().addReporter(
		new JSONReporter({
			file: `tests/${caps.get("browserName")}-results.json`,
			beautify: true,
			indentationLevel: 4
		})
	);

	/* setup jasmine support for plugin */
	require("./jasmine");
}

function afterLaunch() {
	server.close();
}

exports.config = {
	suites: {
		all: "tests/test.spec.js"
	},

	baseUrl: undefined /* set from onPrepare */,
	framework: "jasmine2",
	directConnect: true,

	capabilities: {
		browserName: "chrome",
		"moz:firefoxOptions": {
			args: ["-headless"]
		},
		chromeOptions: {
			args: ["--no-sandbox", "--disable-gpu", "--headless"]
		}
	},

	plugins: [{ path: "index.js" }],

	beforeLaunch,
	onPrepare,
	afterLaunch
};
