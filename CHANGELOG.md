# protractor-html-validate changelog

## [1.6.2](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.6.1...v1.6.2) (2019-08-20)

### Bug Fixes

- **deps:** update dependency deepmerge to v4 ([e9abb3f](https://gitlab.com/html-validate/protractor-html-validate/commit/e9abb3f))

## 1.6.1 (2019-06-07)

- Bump `html-validate` to 1.1.0

## 1.6.0 (2019-02-17)

- Bump `html-validate` to 0.21.0
- Bump dependencies.

## 1.5.0 (2019-01-29)

- Bump `html-validate` to 0.20.0

## 1.4.0 (2019-01-20)

- Run tests against firefox as well.
- Use `codeframe` formatter to give more context.
- Bump `html-validate` to 0.17.0.

## 1.3.0 (2018-11-21)

- Extend `htmlvalidate:document` by default.
- Bump `html-validate to 0.15.0.
- Bump other dependencies.

## 1.2.2 (2018-10-22)

- Bump `html-validate` to 0.13.0.

## 1.2.1 (2018-10-14)

- Bump `html-validate` to 0.11.1.
- Bump other dependencies.

## 1.2.0 (2018-09-22)

- Change errors so all errors are listed, showing line and column information
  and prefixing with `html-validate` so the source of the errors are easier to
  identify.
- Disable `no-inline-style` by default as some frameworks rely on inline style
  which causes many false positives.

## 1.1.1

- Fix configuration merging.

## 1.1.0

- Bump `html-validate` to 0.10.0

## 1.0.0

- Initial version
