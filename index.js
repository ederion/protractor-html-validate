const HtmlValidate = require("html-validate").HtmlValidate;
const deepmerge = require("deepmerge");

const defaultConfig = require("./config");

class ProtractorHtmlValidate {
	constructor() {
		this.config = undefined; /* set by protractor */
		this.htmlvalidate = undefined;
	}

	setup() {
		const config = deepmerge(defaultConfig, this.config.config || {});
		this.htmlvalidate = new HtmlValidate(config);

		/* extend browser object with htmlvalidate function */
		browser.htmlvalidate = () => {
			return protractor.promise
				.all([browser.getCurrentUrl(), browser.getPageSource()])
				.then(([url, source]) => {
					const report = this.htmlvalidate.validateString(source);

					/* remap "inline" filename to url */
					report.results = report.results.map(result => {
						result.filePath = url;
						return result;
					});

					return report;
				});
		};
	}

	onPageStable() {
		/* validate page on each page load */
		if (jasmine) {
			expect(browser.htmlvalidate()).toBeValid("when loading page");
		}
	}

	// eslint-disable-next-line consistent-return
	postTest(pass, testInfo) {
		/* add a final validation after each test is finished */
		if (!jasmine) {
			return browser.htmlvalidate().then(report => {
				this._addResults(report, `${testInfo.category} ${testInfo.name}`);
			});
		}
	}

	_validate(browser) {
		return browser.getPageSource().then(source => {
			return this.htmlvalidate.validateString(source);
		});
	}

	_addResults(report, spec) {
		const info = { specName: spec };
		if (!report.valid) {
			for (const message of report.results[0].messages) {
				const text = `${message.message} [${message.ruleId}]`;
				switch (message.severity) {
					case 2:
						this.addFailure(text, info);
						break;
					case 1:
						this.addWarning(text, info);
						break;
				}
			}
		}
	}
}

module.exports = new ProtractorHtmlValidate();
